import { defineStore } from 'pinia'
import axios from '../axios'

export const useUserStore = defineStore('user', {
  state: () => ({
    authenticated: null as boolean | null,
    email: null,
    name: null,
    phone: null,
    dob: null,
    address: null,
    userList: []
  }),
  actions: {
    setAuthenticated(authed: boolean) {
      this.authenticated = authed
    },
    async getUserDetail() {
      try {
        const userDetail = await axios.get('/user')
        this.authenticated = true
        this.email = userDetail.data.email
        this.name = userDetail.data.name
        this.phone = userDetail.data.phone
        this.dob = userDetail.data.dob
        this.address = userDetail.data.address

      } catch (err: any) {
        console.log(err)
        if (err?.response?.data?.err === 'Unauthorized') {
          this.authenticated = false
        }
      }
      return this.authenticated
    },
    async getUserList() {
      try {
        const userList = await axios.get('/user/list')
        this.authenticated = true
        this.userList = userList.data

      } catch (err: any) {
        console.log(err)
        if (err?.response?.data?.err === 'Unauthorized') {
          this.authenticated = false
        }
      }
    }
  }
})
