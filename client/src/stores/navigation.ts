import { defineStore } from 'pinia'

export const useNavigationStore = defineStore('navigation', {
  state: () => ({
    tab: 'my'
  }),
  actions: {
    async setTab(tab: string) {
      this.tab = tab
    }
  }
})
