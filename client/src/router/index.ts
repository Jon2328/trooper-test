import { createRouter, createWebHistory } from 'vue-router'
import Login from '../views/Login.vue'
import { useUserStore } from '@/stores/user'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'about',
      component: () => import('../views/Register.vue')
    },
    {
      path: '/profile',
      name: 'profile',
      component: () => import('../views/Profile.vue'),
      beforeEnter: [loginGuard, profileGuard]
    },
    {
      path: '/profile-creation',
      name: 'profile-creation',
      component: () => import('../views/ProfileCreation.vue'),
      beforeEnter: loginGuard
    }
  ]
})

async function loginGuard(to: any, from: any, next: any) {
  const userStore = useUserStore()
  if (userStore.authenticated === null) {
    const authState = await userStore.getUserDetail()
    if (!authState) {
      next({ path: '/' })
    } else {
      next()
    }
  } else if (userStore.authenticated) {
    next()
  } else {
    next({ path: '/' })
  }
}

async function profileGuard(to: any, from: any, next: any) {
  const userStore = useUserStore()
  await userStore.getUserDetail()
  console.log(userStore.name)
  if (userStore.name === null) {
    console.log('create')
    next({ path: '/profile-creation' })
  } else {
    next()
  }
}



export default router
